#include <math.h>
#include <iostream>
#include "CImg.h"
#include "iterletter.cpp"

using namespace cimg_library;
using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cerr << "Supply a picture!" << endl;
		return 1;
	}
	IterLetter word = IterLetter("peace");
	if (argc > 2) {
		word = IterLetter(argv[2]);
	}
	char *file_in = argv[1];
	CImg<float> img(file_in);
	const int ROWS = img.height();
	double poi = img.width();
	poi /= (word.getLength()+1);
	poi *= 2;
	const int COLS = (int)floor(poi);
	for (int row = 0; row < ROWS; ++row) {
		int col = 0;
		int k = 0;
		for (int j = 0; j < COLS; ++j) {
			while (word.hasNext()) {
				const float r = img(col,row,0);
				const float g = img(col,row,1);
				const float b = img(col,row,2);
				cout << "\033[38;2;" << (int)r << ";" << (int)g << ";" << (int)b << "m" << word.getNext();
				if (k % 2 == 1) ++col;
				++k;
			}
			word.restart();
			if (j < COLS-1) {
				cout << " ";
				if (k % 2 == 1) ++col;
				++k;
			}
		}
		cout << "\n";
	}
}
