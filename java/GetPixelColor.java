import java.io.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
public class GetPixelColor {
    public static void main(String args[]) throws IOException {
        File file = new File(args[0]);
        BufferedImage image = ImageIO.read(file);
        IterLetter word = new IterLetter("helooooo");
        if (args.length > 1) {
            word = new IterLetter(args[1]);
        }
        final int ROWS = image.getHeight(); 
        double poi = (double)image.getWidth();
        poi /= (word.getLength()+1);
        poi *= 2;
        final int COLS = (int)Math.floor(poi);
		System.out.println(image.getWidth() + "x" + image.getHeight());
		System.out.println(COLS);
        for (int row = 0; row < ROWS; ++row) {
            int col = 0;
            int k = 0;
            for (int j = 0; j < COLS; ++j) {
                while (word.hasNext()) {
                    int clr = image.getRGB(col, row);
                    int red =   (clr & 0x00ff0000) >> 16;
                    int green = (clr & 0x0000ff00) >> 8;
                    int blue =   clr & 0x000000ff;
                    System.out.print("\033[38;2;" + red + ";" + green + ";" + blue + "m" + word.getNext());
                    if (k % 2 == 1) ++col;
                    ++k;
                }
                word.restart();
                if (j < COLS-1) {
                    System.out.print(" ");
                    if (k % 2 == 1) ++col;
                    ++k;
                }
            }
            System.out.println("\033[0m");
        }
    }
}
