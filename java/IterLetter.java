public class IterLetter {
    private String str;
    private int index = 0;
    public IterLetter(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        this.str = str;
    }
    public void printNext() {
        if (hasNext()) {
            System.out.println(str.charAt(index)); 
            ++index;
        } 
    }

    public char getNext() {
        if (hasNext()) {
            return this.str.charAt(index++);
        } 
        return '\0';
    }

    public boolean hasNext() {
        return str.length() > index;
    }

    public void restart() {
        index = 0;
    }

    public int getLength() {
        return str.length();
    }
}
