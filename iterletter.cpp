#pragma once

#include <string>
#include <type_traits>

class IterLetter {
    private:
        std::string str;
        long unsigned int idx = 0;

    public:
        IterLetter(std::string str) {
            this->str = str;
        }

        char getNext() {
            if (hasNext()) {
                return str[idx++];
            }
            return '\0';
        }

        bool hasNext() {
            return str.length() > idx;;
        }

       void restart() {
           idx = 0;
       }

       int getLength() {
           return str.length();
       }
};
